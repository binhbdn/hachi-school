const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');
mix .sass('resources/sass/bootstrap-4.5.3/custom-bootstrap.scss', 'public/css/custom-bootstrap-4.5.3.min.css')
    .sass('resources/sass/website/home.scss', 'public/css/website/custom-home.min.css')
    ;
