@include('website.layouts.banner')

@include('website.layouts.features')

@include('website.layouts.about')

@include('website.layouts.categories')

@include('website.layouts.courses')

@include('website.layouts.cta')

@include('website.layouts.choose')

@include('website.layouts.testimonial')

@include('website.layouts.gallery')

@include('website.layouts.team')

@include('website.layouts.blog')

@include('website.layouts.partner')

@include('website.layouts.newsletter')