@extends('website.layouts.default')
@section('css-custom')
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="/home/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="/home/css/responsive.css">
@endsection
@section('content')
    @include('website.content.news_sub1_list')
@endsection
@section('modal-custom')
<!-- Custom Modal Start -->
<!-- Custom Modal End -->
@endsection
@section('js-custom')
    <script>
        $(document).ready(function() {
            $('a[href$="/tin-tuc"]').parent('li').addClass('current-menu-item');
        });
    </script>
@endsection