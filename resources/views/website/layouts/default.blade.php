<!doctype html>
<html lang="{{Session::get('locale')}}">
<head>
    <!-- meta tag -->
    <meta charset="utf-8" />
    <!-- responsive tags -->
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- title tag -->
    <title>{{$settings->title}} | {{trans($breadcrumb->title)}}</title>

    <!-- meta tags -->
    <meta name="description" content="{{$settings->description}}" />
    <meta name="author" content="{{$settings->description}}" />
    <meta name="copyright" content="{{$settings->description}}" />
    <meta name="keyword" content="{{$settings->keywords}}" />
    <meta name="keywords" content="{{$settings->keywords}}" />
    <meta name="robots" content="noodp,index,follow" />
    <meta name="mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <!-- bootstrap v4.4.1 css -->
    <link rel="stylesheet" type="text/css" href="/home/css/bootstrap.min.css">
    <!-- font-awesome css -->
    <link rel="stylesheet" type="text/css" href="/home/css/font-awesome.min.css">
    <!-- flag-icon css -->
    <link rel="stylesheet" type="text/css" href="/home/css/flag-icon-css/flag-icon.min.css">
    <!-- animate css -->
    <link rel="stylesheet" type="text/css" href="/home/css/animate.css">
    <!-- owl.carousel css -->
    <link rel="stylesheet" type="text/css" href="/home/css/owl.carousel.css">
    <!-- slick css -->
    <link rel="stylesheet" type="text/css" href="/home/css/slick.css">
    <!-- linea-font css -->
    <link rel="stylesheet" type="text/css" href="/home/fonts/linea-fonts.css">
    <!-- flaticon css  -->
    <link rel="stylesheet" type="text/css" href="/home/fonts/flaticon.css">
    <!-- magnific popup css -->
    <link rel="stylesheet" type="text/css" href="/home/css/magnific-popup.css">
    <!-- main menu css -->
    <link rel="stylesheet" href="/home/css/rsmenu-main.css">
    <!-- spacing css -->
    <link rel="stylesheet" type="text/css" href="/home/css/rs-spacing.css">
@yield('css-custom')

    <!-- favicon -->
    <link rel="icon" type="image/x-icon" href="/home/images/logo/fav.png" />
    <link rel="shortcut icon" type="image/x-icon" href="/home/images/logo/fav.png" />
    <link rel="canonical" href="{{url()->current()}}" />
    <link rel="apple-touch-icon" href="/home/images/logo/apple-touch-icon.png" />

    <!-- meta tags for Facebook -->
    <meta property="og:site_name" content="{{$settings->title}}" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:title" content="{{$settings->title}}" />
    <meta property="og:description" content="{{$settings->description}}" />
    <meta property="og:type" content="{{$settings->type}}" />
    <meta property="og:locale" content="{{$settings->locale}}" />
    <meta property="og:image" content="{{asset('upload/'.$settings->fb_link_img)}}" />
    <meta property="og:image:secure_url" content="{{asset('upload/'.$settings->fb_link_img)}}" />
    <meta property="og:image:width" content="750" />
    <meta property="og:image:height" content="150" />
    <meta property="fb:app_id" content="{{$settings->fb_app_id}}" />

    <!-- meta tags for Twitter -->
    <meta name="twitter:site" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:creator" content="" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:image" content="" />
    <!-- Hide 000Webhost Brand -->
    <style>
        img[src*="https://cdn.000webhost.com/000webhost/logo/footer-powered-by-000webhost-white2.png"] {
            display: none;
        }
    </style>
</head>
<body class="home-style4 px-0 translator-{{Session::get('locale')}}">
@include('website.layouts.preloader')

@include('website.layouts.header')

<!-- Main content Start -->
<div class="main-content">
    @yield('content')
</div> 
<!-- Main content End --> 

@include('website.layouts.footer')

@include('website.layouts.scrollup')

@include('website.layouts.search_modal')

@yield('modal-custom')

@include('website.layouts.script')

@yield('js-custom')
</body>
</html>