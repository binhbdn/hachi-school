<!-- Login Section Start -->
<div class="rs-login pt-50 pb-50 md-pt-30 md-pb-30">
    <div class="container">
        <div class="noticed">
            <div class="main-part">                           
                <div class="method-account">
                    <h2 class="login">{{trans('admin.login')}}</h2>
                    <form>
                        <input type="text" name="username_email" placeholder="{{trans('home.new_account.username')}} | {{trans('home.contact.emailaddresss')}}" required>
                        <input type="text" name="password" placeholder="{{trans('admin.password')}}" required>
                        <button type="submit" class="hachi-btn mb-12" data-toggle="tooltip" data-placement="top" title="{{trans('admin.login')}}">{{trans('admin.login')}}</button>
                        <div class="last-password">
                            <p>{{trans('home.login.not_registered')}} <a href="/dang-ky">{{trans('home.login.create_account')}}</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Login Section End -->  