    <!-- Team Section Start -->
    <div id="rs-team" class="rs-team home11-style gray-bg3 pt-50 pb-50 md-pt-30 md-pb-30">
        <div class="container">
            <div class="sec-title2 text-center mb-30 md-mb-15">
                <!-- <div class="sub-title">{{trans('home.team.sub-title')}}</div> -->
                <h2 class="title">{{trans('home.team.title')}}</h2>
            </div>
            <div class="rs-carousel owl-carousel nav-style2" data-loop="true" data-items="4" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="true" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="4" data-md-device-nav="true" data-md-device-dots="false">
                <div class="team-item">
                    <div class="team-thumbnail">
                        <div class="team-img">
                            <img src="/home/images/team/1.jpg" alt="">
                            <ul class="team-social-link">
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-header">
                            <h4 class="name"><a href="#">{{trans('home.team.name1')}}</a></h4>
                            <span class="subject">{{trans('home.team.pos1')}}</span>
                        </div>
                    </div>
                </div> 
                <div class="team-item">
                    <div class="team-thumbnail">
                        <div class="team-img">
                            <img src="/home/images/team/2.jpg" alt="">
                            <ul class="team-social-link">
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-header">
                            <h4 class="name"><a href="#">{{trans('home.team.name2')}}</a></h4>
                            <span class="subject">{{trans('home.team.pos2')}}</span>
                        </div>
                    </div>
                </div>
                <div class="team-item">
                    <div class="team-thumbnail">
                        <div class="team-img">
                            <img src="/home/images/team/3.jpg" alt="">
                            <ul class="team-social-link">
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-header">
                            <h4 class="name"><a href="#">{{trans('home.team.name3')}}</a></h4>
                            <span class="subject">{{trans('home.team.pos3')}}</span>
                        </div>
                    </div>
                </div>
                <div class="team-item">
                    <div class="team-thumbnail">
                        <div class="team-img">
                            <img src="/home/images/team/4.jpg" alt="">
                            <ul class="team-social-link">
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-header">
                            <h4 class="name"><a href="#">{{trans('home.team.name4')}}</a></h4>
                            <span class="subject">{{trans('home.team.pos4')}}</span>
                        </div>
                    </div>
                </div>
                <div class="team-item">
                    <div class="team-thumbnail">
                        <div class="team-img">
                            <img src="/home/images/team/5.jpg" alt="">
                            <ul class="team-social-link">
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-header">
                            <h4 class="name"><a href="#">{{trans('home.team.name5')}}</a></h4>
                            <span class="subject">{{trans('home.team.pos5')}}</span>
                        </div>
                    </div>
                </div> 
                <div class="team-item">
                    <div class="team-thumbnail">
                        <div class="team-img">
                            <img src="/home/images/team/6.jpg" alt="">
                            <ul class="team-social-link">
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-header">
                            <h4 class="name"><a href="#">{{trans('home.team.name6')}}</a></h4>
                            <span class="subject">{{trans('home.team.pos6')}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Team Section End -->