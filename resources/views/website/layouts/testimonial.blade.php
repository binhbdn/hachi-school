    <!-- Testimonial Section Start -->
    <div class="rs-testimonial home11-style pt-50 pb-50 md-pt-30 md-pb-30">
        <div class="container">
            <div class="sec-title2 text-center mb-20">
                <!-- <div class="sub-title">{{trans('home.testimonial.sub-title')}}</div> -->
                <h2 class="title">{{trans('home.testimonial.title')}}</h2>
            </div>
            <div class="rs-carousel owl-carousel" data-loop="true" data-items="2" data-margin="30" data-autoplay="true" data-autoplay-timeout="15000" data-smart-speed="5000" data-dots="true" data-nav="false" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="2" data-md-device-nav="false" data-md-device-dots="true">
                <div class="testi-item">
                    <div class="row">
                        <div class="col-lg-4 md-mb-30 col-md-4">
                            <div class="user-img">
                                <img src="/home/images/testimonial/1.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="user-info">
                                <div class="desc text-justify">{{trans('home.testimonial.comment1')}}</div>
                                <p class="name text-right">{{trans('home.testimonial.name_a1')}}</p>
                                <p class="position text-right">{{trans('home.testimonial.name_b')}}<span>{{trans('home.testimonial.name_c1')}}</span>{{trans('home.testimonial.name_d')}}</p>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="testi-item">
                    <div class="row">
                        <div class="col-lg-4 md-mb-30 col-md-4">
                            <div class="user-img">
                                <img src="/home/images/testimonial/2.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                        <div class="user-info">
                                <div class="desc text-justify">{{trans('home.testimonial.comment2')}}</div>
                                <p class="name text-right">{{trans('home.testimonial.name_a2')}}</p>
                                <p class="position text-right">{{trans('home.testimonial.name_b')}}<span>{{trans('home.testimonial.name_c2')}}</span>{{trans('home.testimonial.name_d')}}</p>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="testi-item">
                    <div class="row">
                        <div class="col-lg-4 md-mb-30 col-md-4">
                            <div class="user-img">
                                <img src="/home/images/testimonial/1.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="user-info">
                                <div class="desc text-justify">{{trans('home.testimonial.comment3')}}</div>
                                <p class="name text-right">{{trans('home.testimonial.name_a3')}}</p>
                                <p class="position text-right">{{trans('home.testimonial.name_b')}}<span>{{trans('home.testimonial.name_c3')}}</span>{{trans('home.testimonial.name_d')}}</p>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="testi-item">
                    <div class="row">
                        <div class="col-lg-4 md-mb-30 col-md-4">
                            <div class="user-img">
                                <img src="/home/images/testimonial/2.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="user-info">
                                <div class="desc text-justify">{{trans('home.testimonial.comment4')}}</div>
                                <p class="name text-right">{{trans('home.testimonial.name_a4')}}</p>
                                <p class="position text-right">{{trans('home.testimonial.name_b')}}<span>{{trans('home.testimonial.name_c4')}}</span>{{trans('home.testimonial.name_d')}}</p>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="testi-item">
                    <div class="row">
                        <div class="col-lg-4 md-mb-30 col-md-4">
                            <div class="user-img">
                                <img src="/home/images/testimonial/1.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="user-info">
                                <div class="desc text-justify">{{trans('home.testimonial.comment5')}}</div>
                                <p class="name text-right">{{trans('home.testimonial.name_a5')}}</p>
                                <p class="position text-right">{{trans('home.testimonial.name_b')}}<span>{{trans('home.testimonial.name_c5')}}</span>{{trans('home.testimonial.name_d')}}</p>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="testi-item">
                    <div class="row">
                        <div class="col-lg-4 md-mb-30 col-md-4">
                            <div class="user-img">
                                <img src="/home/images/testimonial/2.png" alt="">
                            </div>
                        </div>
                        <div class="col-lg-8 col-md-8">
                            <div class="user-info">
                                <div class="desc text-justify">{{trans('home.testimonial.comment6')}}</div>
                                <p class="name text-right">{{trans('home.testimonial.name_a6')}}</p>
                                <p class="position text-right">{{trans('home.testimonial.name_b')}}<span>{{trans('home.testimonial.name_c6')}}</span>{{trans('home.testimonial.name_d')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Testimonial Section End -->