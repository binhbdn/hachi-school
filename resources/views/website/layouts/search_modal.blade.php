<!-- Search Modal Start -->
<div class="mx-auto" style="width:40%; min-width: 300px;">
<div aria-hidden="true" class="modal fade search-modal pl-15" role="dialog" tabindex="-1" style="width:40%; min-width: 300px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="search-block clearfix">
                <form>
                    <div class="form-group mb-0">
                        <input class="form-control" placeholder="{{trans('home.searchhere')}}" type="text">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Search Modal End -->