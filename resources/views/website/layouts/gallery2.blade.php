<!-- Gallery Section Start -->
<div class="rs-gallery pt-50 pb-50 md-pt-30 md-pb-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="/home/images/gallery/1.jpg"><img src="/home/images/gallery/1.jpg" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title1')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="/home/images/gallery/2.jpg"><img src="/home/images/gallery/2.jpg" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title2')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="/home/images/gallery/3.jpg"><img src="/home/images/gallery/3.jpg" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title3')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="https://loremflickr.com/390/310"><img src="https://loremflickr.com/390/310" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title4')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="https://loremflickr.com/429/341"><img src="https://loremflickr.com/429/341" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title5')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="https://loremflickr.com/468/372"><img src="https://loremflickr.com/468/372" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title6')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="https://loremflickr.com/507/403"><img src="https://loremflickr.com/507/403" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title7')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="https://loremflickr.com/546/434"><img src="https://loremflickr.com/546/434" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title8')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="https://loremflickr.com/585/465"><img src="https://loremflickr.com/585/465" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title9')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 col-md-6 md-mb-30">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="https://loremflickr.com/624/496"><img src="https://loremflickr.com/624/496" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title10')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 col-md-6 sm-md-30">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="https://loremflickr.com/663/527"><img src="https://loremflickr.com/663/527" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title11')}}</div>
                </div>
            </div>  
            <div class="col-lg-4 col-md-6">
                <div class="gallery-item">
                    <div class="gallery-img">
                        <a class="image-popup" href="https://loremflickr.com/702/558"><img src="https://loremflickr.com/702/558" alt=""></a>
                    </div>
                    <div class="title">{{trans('home.gallery.title12')}}</div>
                </div>
            </div>
        </div>
    </div> 
</div>
<!-- Gallery Section End -->