    <!-- About Section Start -->
    <div id="rs-about" class="rs-about style11 pb-20 md-pb-10">
        <div class="container">
            <div class="row md-mt-30">
                <div class="col-lg-6 col-md-12 md-mb-30">
                    <div class="img-part js-tilt">
                        <img src="/home/images/about/about.png" alt="images">
                    </div>
                </div>
                <div class="col-lg-6 pl-65 md-pl-15 col-md-12">
                    <div class="sec-title2">
                        <!-- <div class="sub-title">{{trans('home.about.sub-title')}}</div> -->
                        <h2 class="title mb-30 md-mb-15">{{trans('home.about.title')}}</h2>
                        <p class="desc text-justify mb-75">{{trans('home.about.txt1')}}</p>
                    </div>
                    <a class="hachi-btn" href="#" data-toggle="tooltip" data-placement="top" title="{{trans('home.about.read_more')}}">{{trans('home.about.read_more')}}</a>
                </div>
            </div>
        </div>
    </div>
    <!-- About Section End -->