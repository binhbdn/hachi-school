    <!-- Categories Section Start -->
    <div id="rs-categories" class="rs-categories home11-style container pb-50 md-pb-30">
        <div class="row pl-15 pr-15">
            <div class="col-lg-5 img-part padding-0">
            </div>
            <div class="col-lg-7 padding-0">
                <div class="main-part">
                    <div class="sec-title2 mb-40 md-mb-15">
                        <!-- <div class="sub-title">{{trans('home.categories.sub-title')}}</div> -->
                        <h2 class="title">{{trans('home.categories.title')}}</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 mb-20">
                            <div class="categories-item">
                                <div class="icon-part">
                                    <img src="/home/images/categories/icons/2.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="#">{{trans('home.categories.content1')}}</a></h4>
                                    <p class="text-justify">{{trans('home.categories.txt1')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-20">
                            <div class="categories-item">
                                <div class="icon-part">
                                    <img src="/home/images/categories/icons/4.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="#">{{trans('home.categories.content2')}}</a></h4>
                                    <p class="text-justify">{{trans('home.categories.txt2')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-20">
                            <div class="categories-item">
                                <div class="icon-part">
                                    <img src="/home/images/categories/icons/3.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="#">{{trans('home.categories.content3')}}</a></h4>
                                    <p class="text-justify">{{trans('home.categories.txt3')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 mb-20">
                            <div class="categories-item">
                                <div class="icon-part">
                                    <img src="/home/images/categories/icons/1.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="#">{{trans('home.categories.content4')}}</a></h4>
                                    <p class="text-justify">{{trans('home.categories.txt4')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 sm-mb-20">
                            <div class="categories-item">
                                <div class="icon-part">
                                    <img src="/home/images/categories/icons/6.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="#">{{trans('home.categories.content5')}}</a></h4>
                                    <p class="text-justify">{{trans('home.categories.txt5')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="categories-item">
                                <div class="icon-part">
                                    <img src="/home/images/categories/icons/5.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title"><a href="#">{{trans('home.categories.content6')}}</a></h4>
                                    <p class="text-justify">{{trans('home.categories.txt6')}}</p>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <!-- Categories Section End -->