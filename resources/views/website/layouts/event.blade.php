<!-- Events Section Start -->
<div class="rs-event modify1 orange-color pt-50 pb-50 md-pt-30 md-pb-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="event-item">
                    <div class="event-short">
                        <div class="featured-img">
                            <img src="https://loremflickr.com/1000/600" alt="Image">
                            <div class="dates">24 Tháng 7, 2020</div>
                        </div>
                        <div class="content-part">
                            <h4 class="title"><a href="#">{{trans('home.blog.event1')}}</a></h4>
                            <div class="time-sec">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 11:00 AM -  03:00 AM </div>
                                <div class="address"><i class="fa fa-map-o"></i> Vinh, Nghệ An</div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="event-item">
                    <div class="event-short">
                        <div class="featured-img">
                            <img src="https://loremflickr.com/990/594" alt="Image">
                            <div class="dates">24 Tháng 7, 2020</div>
                        </div>
                        <div class="content-part">
                            <h4 class="title"><a href="#">{{trans('home.blog.event2')}}</a></h4>
                            <div class="time-sec">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 11:00 AM -  03:00 AM </div>
                                <div class="address"><i class="fa fa-map-o"></i> Vinh, Nghệ An</div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="event-item">
                    <div class="event-short">
                        <div class="featured-img">
                            <img src="https://loremflickr.com/980/588" alt="Image">
                            <div class="dates">24 Tháng 7, 2020</div>
                        </div>
                        <div class="content-part">
                            <h4 class="title"><a href="#">{{trans('home.blog.event4')}}</a></h4>
                            <div class="time-sec">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 11:00 AM -  03:00 AM </div>
                                <div class="address"><i class="fa fa-map-o"></i> Vinh, Nghệ An</div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="event-item">
                    <div class="event-short">
                        <div class="featured-img">
                            <img src="https://loremflickr.com/970/582" alt="Image">
                            <div class="dates">24 Tháng 7, 2020</div>
                        </div>
                        <div class="content-part">
                            <h4 class="title"><a href="#">{{trans('home.blog.event2')}}</a></h4>
                            <div class="time-sec">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 11:00 AM -  03:00 AM </div>
                                <div class="address"><i class="fa fa-map-o"></i> Vinh, Nghệ An</div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="event-item">
                    <div class="event-short">
                        <div class="featured-img">
                            <img src="https://loremflickr.com/960/576" alt="Image">
                            <div class="dates">24 Tháng 7, 2020</div>
                        </div>
                        <div class="content-part">
                            <h4 class="title"><a href="#">{{trans('home.blog.event4')}}</a></h4>
                            <div class="time-sec">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 11:00 AM -  03:00 AM </div>
                                <div class="address"><i class="fa fa-map-o"></i> Vinh, Nghệ An</div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-30 col-md-6">
                <div class="event-item">
                    <div class="event-short">
                        <div class="featured-img">
                            <img src="https://loremflickr.com/950/570" alt="Image">
                            <div class="dates">24 Tháng 7, 2020</div>
                        </div>
                        <div class="content-part">
                            <h4 class="title"><a href="#">{{trans('home.blog.event1')}}</a></h4>
                            <div class="time-sec">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 11:00 AM -  03:00 AM </div>
                                <div class="address"><i class="fa fa-map-o"></i> Vinh, Nghệ An</div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 md-mb-30">
                <div class="event-item">
                    <div class="event-short">
                        <div class="featured-img">
                            <img src="https://loremflickr.com/940/564" alt="Image">
                            <div class="dates">24 Tháng 7, 2020</div>
                        </div>
                        <div class="content-part">
                            <h4 class="title"><a href="#">{{trans('home.blog.event4')}}</a></h4>
                            <div class="time-sec">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 11:00 AM -  03:00 AM </div>
                                <div class="address"><i class="fa fa-map-o"></i> Vinh, Nghệ An</div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 sm-mb-30">
                <div class="event-item">
                    <div class="event-short">
                        <div class="featured-img">
                            <img src="https://loremflickr.com/930/558" alt="Image">
                            <div class="dates">24 Tháng 7, 2020</div>
                        </div>
                        <div class="content-part">
                            <h4 class="title"><a href="#">{{trans('home.blog.event1')}}</a></h4>
                            <div class="time-sec">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 11:00 AM -  03:00 AM </div>
                                <div class="address"><i class="fa fa-map-o"></i> Vinh, Nghệ An</div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="event-item">
                    <div class="event-short">
                        <div class="featured-img">
                            <img src="https://loremflickr.com/920/552" alt="Image">
                            <div class="dates">24 Tháng 7, 2020</div>
                        </div>
                        <div class="content-part">
                            <h4 class="title"><a href="#">{{trans('home.blog.event2')}}</a></h4>
                            <div class="time-sec">
                                <div class="timesec"><i class="fa fa-clock-o"></i> 11:00 AM -  03:00 AM </div>
                                <div class="address"><i class="fa fa-map-o"></i> Vinh, Nghệ An</div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<!-- Events Section End -->