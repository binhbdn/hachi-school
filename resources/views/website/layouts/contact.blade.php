    <!-- Contact Section Start -->
    <div class="contact-page-section pt-50 pb-50 md-pt-30 md-pb-30">
        <div class="container">
            <div class="row rs-contact-box pb-50 md-pb-30">
                <div class="col-lg-4 col-md-12 md-mb-30">
                    <div class="address-item">
                        <div class="icon-part">
                            <img src="home/images/contact/icon/1.png" alt="">
                        </div>
                        <div class="address-text">
                            <span class="label">{{trans('home.footer.address.title')}}</span>
                            <span class="des">{{trans('home.footer.address.address')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 md-mb-30">
                    <div class="address-item">
                        <div class="icon-part">
                            <img src="home/images/contact/icon/2.png" alt="">
                        </div>
                        <div class="address-text">
                            <span class="label">{{trans('home.contact.emailaddresss')}}</span>
                            <span class="des"><a href="mailto:{{trans('home.footer.address.email')}}">{{trans('home.footer.address.email')}}</a></span>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4 col-md-12">
                    <div class="address-item">
                        <div class="icon-part">
                            <img src="home/images/contact/icon/3.png" alt="">
                        </div>
                        <div class="address-text">
                            <span class="label">{{trans('home.contact.phonenumber')}}</span>
                            <span class="des"><a href="tel:{{trans('home.footer.address.tel')}}">{{trans('home.footer.address.tel')}}</a></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-6 md-mb-50">
                    <!-- Map Section Start --> 
                    <div class="contact-map3">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3779.765641133892!2d105.6762367144094!3d18.674509769323063!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3139ce706fd0344b%3A0x6f0b06a681d64b03!2zODcgTmd1eeG7hW4gVGjhu4sgTWluaCBLaGFpLCBIxrBuZyBCw6xuaCwgVGjDoG5oIHBo4buRIFZpbmgsIE5naOG7hyBBbiwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1606964092890!5m2!1svi!2s" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
                <div class="col-lg-6 md-mb-30 pl-35 md-pl-15">
                    <div class="contact-comment-box">
                        <div class="sec-title2">
                            <h2 class="title mb-mb-15">{{trans('home.contact.quickcontact')}}</h2>
                            <p>{{trans('home.contact.txt')}}</p>
                        </div>
                        <div id="form-messages"></div>
                        <form id="contact-form" method="post" action="mailer.php">
                            <div class="row">
                                <div class="col-lg-6 mb-25 col-md-6 col-sm-6">
                                    <input class="from-control" type="text" id="name" name="name" placeholder="{{trans('home.contact.name')}}" required>
                                </div> 
                                <div class="col-lg-6 mb-25 col-md-6 col-sm-6">
                                    <input class="from-control" type="text" id="name" name="email" placeholder="{{trans('home.contact.emailaddresss')}}" required>
                                </div>   
                                <div class="col-lg-6 mb-25 col-md-6 col-sm-6">
                                    <input class="from-control" type="text" id="name" name="phone" placeholder="{{trans('home.contact.phonenumber')}}" required>
                                </div>   
                                <div class="col-lg-6 mb-25 col-md-6 col-sm-6">
                                    <input class="from-control" type="text" id="name" name="subject" placeholder="{{trans('home.contact.subject')}}" required>
                                </div>
                                <div class="col-lg-12 mb-15">
                                    <textarea class="from-control" id="message" name="message" placeholder="{{trans('home.contact.message')}}" required></textarea>
                                </div>
                                <div class="col-lg-12 mb-0">
                                    <div class="text-center">
                                        <input class="hachi-btn" type="submit" value="{{trans('home.contact.submitnow')}}" data-toggle="tooltip" data-placement="top" title="{{trans('home.contact.submitnow')}}">
                                    </div>
                                </div>
                            </div>										   
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact Section End -->