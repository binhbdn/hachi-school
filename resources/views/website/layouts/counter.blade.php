    <!-- Counter Section Start -->
    <div id="rs-about" class="rs-about style3 pt-50 pb-50 md-pt-30 md-pb-30">
        <div class="container">
            <div class="row y-middle">
                <div class="col-lg-4 lg-pr-0 md-mb-30">
                    <div class="about-intro">
                        <div class="sec-title text-justify">
                            <h2 class="title mb-21">{{trans('home.counter.title')}}</h2>
                            <div class="desc big">{{trans('home.counter.txt0')}}</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 pl-83 md-pl-15">
                    <div class="row rs-counter couter-area">
                        <div class="col-md-4 text-center sm-mb-30">
                            <div class="counter-item one">
                                <img class="count-img pb-15" src="home/images/about/icons/1.png" alt="">
                                <h2 class="number rs-count percent">99</h2>
                                <h4 class="title mb-0" style="margin-left: -50px;">{{trans('home.counter.txt1')}}</h4>
                            </div>
                        </div>
                        <div class="col-md-4 text-center sm-mb-30">
                            <div class="counter-item two">
                                <img class="count-img pb-15" src="home/images/about/icons/2.png" alt="">
                                <h2 class="number rs-count percent">10</h2>
                                <h4 class="title mb-0" style="margin-left: -50px;">{{trans('home.counter.txt2')}}</h4>
                            </div>
                        </div>
                        <div class="col-md-4 text-center sm-mb-30">
                            <div class="counter-item three">
                                <img class="count-img pb-15" src="home/images/about/icons/3.png" alt="">
                                <h2 class="number rs-count percent">75</h2>
                                <h4 class="title mb-0" style="margin-left: -50px;">{{trans('home.counter.txt3')}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Counter Section End -->