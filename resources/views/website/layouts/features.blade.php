    <!-- Features Section start -->
    <div id="rs-features" class="rs-features style3 pt-50 pb-50 md-pt-30 md-pb-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 md-mb-30 sm-mb-15">
                    <div class="features-item text-center">
                        <img src="/home/images/features/bg/5.png" alt="images">                            
                        <div class="content-part">
                            <div class="icon-part">
                                <img src="/home/images/features/icons/1.png" alt="">
                            </div>
                            <h4 class="title">{{trans('home.features.title1')}}</h4>
                            <p>{{trans('home.features.txt1')}}</p>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-3 col-md-6 md-mb-30 sm-mb-15">
                    <div class="features-item text-center">
                        <img src="/home/images/features/bg/5.png" alt="images">                            
                        <div class="content-part">
                            <div class="icon-part">
                                <img src="/home/images/features/icons/2.png" alt="">
                            </div>
                            <h4 class="title">{{trans('home.features.title2')}}</h4>
                            <p>{{trans('home.features.txt2')}}</p>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-3 col-md-6 sm-mb-15">
                    <div class="features-item text-center">
                        <img src="/home/images/features/bg/5.png" alt="images">                            
                        <div class="content-part">
                            <div class="icon-part">
                                <img src="/home/images/features/icons/3.png" alt="">
                            </div>
                            <h4 class="title">{{trans('home.features.title3')}}</h4>
                            <p>{{trans('home.features.txt3')}}</p>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-3 col-md-6">
                    <div class="features-item text-center">
                        <img src="/home/images/features/bg/5.png" alt="images">                            
                        <div class="content-part">
                            <div class="icon-part">
                                <img src="/home/images/features/icons/4.png" alt="">
                            </div>
                            <h4 class="title">{{trans('home.features.title4')}}</h4>
                            <p>{{trans('home.features.txt4')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Features Section End -->