    <!-- Popular Courses Section Start -->
    <div id="rs-popular-courses" class="rs-popular-courses home11-style pb-50 md-pb-30">
        <div class="container">
            <div class="sec-title2 text-center">
                <!-- <div class="sub-title">{{trans('home.courses.sub-title')}}</div> -->
                <h2 class="title">{{trans('home.courses.title')}}</h2>
            </div>
            <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="true" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="3" data-md-device-nav="false" data-md-device-dots="false">
                <div class="courses-item">
                    <div class="img-part">
                        <img src="/home/images/courses/1.jpg" alt="">
                    </div>
                    <div class="content-part text-center">
                        <div class="course-body">
                            <h3 class="title"><a href="#">{{trans('home.courses.course1')}}</a></h3>
                            <p class="text-justify">{{trans('home.courses.intro1')}}</p>
                        </div>
                    </div>
                </div>
                <div class="courses-item">
                    <div class="img-part">
                        <img src="/home/images/courses/2.jpg" alt="">
                    </div>
                    <div class="content-part text-center">
                        <div class="course-body">
                            <h3 class="title"><a href="#">{{trans('home.courses.course2')}}</a></h3>
                            <p class="text-justify">{{trans('home.courses.intro2')}}</p>
                        </div>
                    </div>
                </div>
                <div class="courses-item">
                    <div class="img-part">
                        <img src="/home/images/courses/3.jpg" alt="">
                    </div>
                    <div class="content-part text-center">
                        <div class="course-body">
                            <h3 class="title"><a href="#">{{trans('home.courses.course3')}}</a></h3>
                            <p class="text-justify">{{trans('home.courses.intro3')}}</p>
                        </div>
                    </div>
                </div>
                <div class="courses-item">
                    <div class="img-part">
                        <img src="/home/images/courses/4.jpg" alt="">
                    </div>
                    <div class="content-part text-center">
                        <div class="course-body">
                            <h3 class="title"><a href="#">{{trans('home.courses.course4')}}</a></h3>
                            <p class="text-justify">{{trans('home.courses.intro4')}}</p>
                        </div>
                    </div>
                </div>
                <div class="courses-item">
                    <div class="img-part">
                        <img src="/home/images/courses/5.jpg" alt="">
                    </div>
                    <div class="content-part text-center">
                        <div class="course-body">
                            <h3 class="title"><a href="#">{{trans('home.courses.course5')}}</a></h3>
                            <p class="text-justify">{{trans('home.courses.intro5')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Popular Courses Section End -->