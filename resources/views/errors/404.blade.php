@extends('website.layouts.default')
@section('css-custom')
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="/home/css/style.css">
    <!-- responsive css -->
    <link rel="stylesheet" type="text/css" href="/home/css/responsive.css">
@endsection
@section('content')
    @include('website.layouts.breadcrumbs')
    <!-- 404 Page Start Here -->
    <div id="rs-page-error" class="rs-page-error">
        <div class="error-text">
            <h1 class="error-code">404</h1>
            <h3 class="error-message">{{trans('home.404')}}</h3>
            <a class="hachi-btn" href="/" title="HOME">{{trans('home.to_home_page')}}</a>
        </div>
    </div>
    <!-- 404 Page End Here -->
@endsection