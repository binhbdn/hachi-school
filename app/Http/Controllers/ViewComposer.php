<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use Session;

class ViewComposer
{
    /**
     * The user repository implementation.
     *
     * @var  UserRepository
     */
    protected $settings;

    /**
     * Create a new profile composer.
     *
     * @param    UserRepository  $users
     * @return  void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param    View  $view
     * @return  void
     */
    public function compose(View $view)
    {
        // $settings = null;
        $lang = Session::get('locale');
        // $settings = DB::table('qhv_settings')->select('*')->where('lang', $lang)->get();
        // if (count($settings) == 0){
        //     $settings = (object)[
        //         'title' => 'Quốc Học Vinh',
        //         'shortcut_icon' => 'favicon.png',
        //         'description' => 'Trường THPT Huỳnh Thúc Kháng',
        //         'keywords' => 'Quốc Học Vinh, Trường THPT Huỳnh Thúc Kháng, 62 - Lê Hồng Phong, Thành phố Vinh, Nghệ An',
        //         'fb_app_id' => '944048956027275',
        //         'locale' => 'vi_VN',
        //         'type' => 'educational website',
        //         'fb_link_img' => '2020/06/20200628.png',
        //         'fb_link' => 'thpthuynhthuckhang.nghean/',
        //         'logo' => 'logo.png',
        //         'gtag_id' => 'UA-22927833-35',
        //     ];
        // }else{
        //    $settings = $settings[0];
        // }
        if ($lang == 'vn'){
            $settings = (object)[
                'title' => 'HachiSchool',
                'shortcut_icon' => 'favicon.png',
                'description' => 'HachiSchool',
                'keywords' => 'HachiSchool, 78 - Minh Khai, Thành phố Vinh, Nghệ An',
                'fb_app_id' => '111625737415468',
                'locale' => 'vi_VN',
                'type' => 'educational website',
                'fb_link_img' => '2020/06/20200628.png',
                'fb_link' => 'hachinet.jsc/',
                'logo' => 'logo.png',
                'gtag_id' => 'UA-110986912-1',
            ];
        }else{
            $settings = (object)[
                'title' => 'HachiSchool',
                'shortcut_icon' => 'favicon.png',
                'description' => 'Hachi High School',
                'keywords' => 'Hachi High School, 78 - Minh Khai, Vinh City, Nghe An',
                'fb_app_id' => '111625737415468',
                'locale' => 'vi_VN',
                'type' => 'educational website',
                'fb_link_img' => '2020/06/20200628.png',
                'fb_link' => 'hachinet.jsc/',
                'logo' => 'logo.png',
                'gtag_id' => 'UA-110986912-1',
            ];
        }
        $view->with('settings', $settings);
    }
}